/*
 * Copyright 2021 University of Padua, Italy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package it.unipd.dei.se;


import it.unipd.dei.se.index.ARQMathIndexer;
import it.unipd.dei.se.parse.documents.XMLDocumentParser;
import it.unipd.dei.se.search.ARQMathReRanker;
import it.unipd.dei.se.search.ARQMathSearcher;
import it.unipd.dei.se.analysis.ARQMathAnalyzer;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.search.similarities.BM25Similarity;
import org.apache.lucene.search.similarities.Similarity;

/**
 * ARQMath task 1 Answer Retrieval using <a href="https://lucene.apache.org/" target="_blank">Apache Lucene</a> to index
 * and search the ARQMath Collection corpus. Finally, topics re-ranking is performed.
 *
 * @author GoogolFuel
 * @version 1.0
 * @since 1.0
 */
public class ARQMath {

    /**
     * Main method of the project.
     *
     * @param args command line arguments. If provided, {@code args[0]} contains the path the the index directory;
     *             {@code args[1]} contains the path to the run file.
     * @throws Exception if something goes wrong while indexing, searching or re-ranking.
     */
    public static void main(String[] args) throws Exception {

        // Increase Entity Size Limit to the max value
        System.setProperty("jdk.xml.totalEntitySizeLimit", String.valueOf(Integer.MAX_VALUE));

        final int ramBuffer = 256;
        final String docsPath = "./data/collection/Posts.V1.2.xml";
        final String indexPath = "./experiment/index";
        final int expectedDocs = 1445495;               // number of documents with postTypeId=2 (answers)

        final Analyzer analyzer = new ARQMathAnalyzer();
        final Similarity similarity = new BM25Similarity();

        final String topics = "./data/topics/Topics2021.xml";
        final String tangentSResults = "./data/topics/arqmath-format-results-2021.tsv";
        final String runPath = "./experiment";
        final String runID = "GoogolFuel-task1-S41-auto-both-P";
        final int maxDocsRetrieved = 1000;
        final int expectedTopics = 100;

        final long start = System.currentTimeMillis();
        System.out.printf("#### ARQMath task 1: Start ####%n");

        // Indexing the collection
        final ARQMathIndexer i = new ARQMathIndexer(analyzer, similarity, ramBuffer, indexPath, docsPath, expectedDocs, XMLDocumentParser.class);
        i.index();

        // Searching the topics
        final ARQMathSearcher s = new ARQMathSearcher(analyzer, similarity, indexPath, topics, tangentSResults, expectedTopics, runID, runPath, maxDocsRetrieved);
        s.search();

        // Re-ranking the topics
        final ARQMathReRanker rr = new ARQMathReRanker(docsPath, runPath, runID, expectedTopics, maxDocsRetrieved);
        rr.reRanking(7);

        System.out.printf("%n#### ARQMath task 1: Finished ####%n");
        System.out.printf("ARQMath task 1 finished in %d seconds.%n", (System.currentTimeMillis() - start) / 1000);

    }
}