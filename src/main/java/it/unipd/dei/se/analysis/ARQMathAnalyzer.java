/*
 * Copyright 2021 University of Padua, Italy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package it.unipd.dei.se.analysis;


import org.apache.lucene.analysis.*;
import org.apache.lucene.analysis.charfilter.HTMLStripCharFilter;
import org.apache.lucene.analysis.en.EnglishPossessiveFilter;
import org.apache.lucene.analysis.en.PorterStemFilter;
import org.apache.lucene.analysis.miscellaneous.HyphenatedWordsFilter;
import org.apache.lucene.analysis.standard.StandardTokenizer;

import java.io.IOException;
import java.io.Reader;

import static it.unipd.dei.se.analysis.AnalyzerUtil.consumeTokenStream;

/**
 * Customized ARQMath {@link Analyzer} created by using the {@link StandardTokenizer} and {@link
 * org.apache.lucene.analysis.TokenFilter}s.
 *
 * @author GoogolFuel
 * @version 1.0
 * @since 1.0
 */
public class ARQMathAnalyzer extends Analyzer {

    /**
     * Creates a new instance of the ARQMath analyzer.
     */
    public ARQMathAnalyzer() {
        super();
    }

    @Override
    protected TokenStreamComponents createComponents(String fieldName) {

        final Tokenizer source = new StandardTokenizer();

        TokenStream tokens = new LowerCaseFilter(source);

        tokens = new EnglishPossessiveFilter(tokens);

        tokens = new StopFilter(tokens, AnalyzerUtil.loadStopList("./data/resources/okapi.txt"));

        tokens = new HyphenatedWordsFilter(tokens);

        tokens = new PorterStemFilter(tokens);

        return new TokenStreamComponents(source, tokens);
    }

    @Override
    protected Reader initReader(String fieldName, Reader reader) {
        return new HTMLStripCharFilter(reader);
    }

    @Override
    protected TokenStream normalize(String fieldName, TokenStream in) {
        return new LowerCaseFilter(in);
    }


    /**
     * Main method of the class. Just for testing purposes.
     *
     * @param args command line arguments.
     *
     * @throws IOException if something goes wrong while processing the text.
     */
    public static void main(String[] args) throws IOException {

        // text to analyze
        final String text = "Finding value <span class=of \"math-container\" id=\"q_1\">$c$</span> such that the range of the rational function <span class=\"math-container\" id=\"q_2\">$f(x) = \\frac{x^2 + x + c}{x^2 + 2x + c}$</span> does not contain <span class=\"math-container\" id=\"q_3\">$[-1, -\\frac{1}{3}]$</span>";
        //final String text = "<p>A vertical line in the plane is defined by the equation <span class=\"math-container\" id=\"340331\">x = c</span>, where <span class=\"math-container\" id=\"340332\">c</span> is some real. This makes sense because along the vertical line, the <span class=\"math-container\" id=\"340333\">x</span> value doesn't change. </p>";
        //final String text = "multivariable-calculus,vectors [2280126, 25681, 1079914, 2787917, 2083354, 3050796, 1961529, 1116472, 1265892, 2246212]";

        // use the analyzer to process the text and print diagnostic information about each token
        consumeTokenStream(new ARQMathAnalyzer(), text);

    }

}
