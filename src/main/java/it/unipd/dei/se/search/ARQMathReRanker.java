package it.unipd.dei.se.search;

import it.unipd.dei.se.parse.documents.ParsedDocument;
import it.unipd.dei.se.parse.documents.XMLDocumentParser;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.util.*;

/**
 * Re-ranks the ARQMath topics run for the ARQMath task 1: Answer Retrieval.
 *
 * @author GoogolFuel
 * @version 1.00
 * @since 1.00
 */
public class ARQMathReRanker {

    /**
     * A LOGGER available for the class.
     */
    private static final Logger LOGGER = LogManager.getLogger(ARQMathReRanker.class);

    /**
     * The path of the documents collection.
     */
    private String docsPath;

    /**
     * The path of the run.
     */
    private String runPath;

    /**
     * The identifier of the run.
     */
    private String runID;

    /**
     * The number of topics in the run file.
     */
    private int numTopics;

    /**
     * The number of documents retrieved for each topic.
     */
    private int numDocs;


    /**
     * Creates a new re-ranker for the ARQMath task 1: Answer Retrieval.
     *
     * @param docsPath  the path of the documents collection.
     * @param runPath          the path of the run.
     * @param runID            the identifier of the run.
     * @param numTopics   the number of topics in the run file.
     * @param numDocs the number of documents retrieved for each topic.
     */
    public ARQMathReRanker(final String docsPath, final String runPath, final String runID, final int numTopics, final int numDocs) {
        this.docsPath = docsPath;
        this.runPath = runPath;
        this.runID = runID;
        this.numTopics = numTopics;
        this.numDocs = numDocs;
    }


    /**
     * Re-ranks the specified run. The new rank takes into account the score value of a document, which is provided by
     * the users and stored in the collection. In particular, the new scores are the weighted sum of the run score and
     * the document initial score, where the Lucene run score is considered more important.
     *
     * @param weight integer value representing the weight of the Lucene run score to use in the final score computation.
     *
     * @throws IllegalStateException if something goes wrong while re-ranking topics.
     */
    public void reRanking(final int weight) throws IllegalStateException {

        System.out.printf("%n#### Start re-ranking ####%n");

        // The start time of the re-ranking
        final long start = System.currentTimeMillis();

        try {

            // Create reader and writer
            BufferedReader buffer = new BufferedReader(new FileReader(runPath + "/" + runID + ".tsv"));

            int index = runID.indexOf("-");
            for(int i=0;i<2;i++){
                index = runID.indexOf("-", index + 1);
            }
            String newRunID = runID.substring(0, index) + "R" + weight + "1" + runID.substring(index);
            PrintWriter writer = new PrintWriter(runPath + "/" + newRunID + ".tsv");

            // Get documents scores and put them in a map
            System.out.printf("Get documents score...%n");
            Map<String, Integer> docsScore = new HashMap<>();
            XMLDocumentParser docParser = new XMLDocumentParser(new FileReader(docsPath));
            for (ParsedDocument doc : docParser) {
                if (doc.getPostTypeId().equals("2")) {
                    docsScore.put(doc.getId(), Integer.parseInt(doc.getScore()));
                }
            }

            // Perform run re-ranking
            StringTokenizer st;
            String line, docID, topicID = "", score = "";
            double max_score = 0.0;

            // For each topic in the run
            for (int i = 0; i < numTopics; i++) {

                Map<String, Double> topicValues = new HashMap<>();

                // Get run actual scores and put them in a map
                for (int j = 0; j < numDocs && ((line = buffer.readLine()) != null); j++) {

                    st = new StringTokenizer(line);

                    // Take topicID, docID and related score
                    topicID = st.nextToken();
                    docID = st.nextToken();
                    for (int k = 0; k < 2; k++) {
                        score = st.nextToken();
                    }

                    // Take the maximum score value for the current topic
                    if (j == 0) {
                        max_score =  Double.parseDouble(score);
                    }

                    // Put docID and score in the map
                    topicValues.put(docID, Double.parseDouble(score));
                }

                System.out.printf("Re-ranking topic %s.%n", topicID);

                // Combine Lucene scores with documents scores and put them in a map
                Map<String, Double> newTopicValues = new HashMap<>();
                for (String doc : topicValues.keySet()) {

                    // The Lucene score is normalized, so the new range is [0,1]
                    double norm_run_score = topicValues.get(doc) / max_score;

                    // For this computation, the document score has a maximum value of 10, then it is normalized, so the new range is [0,1]
                    double norm_doc_score = Math.min(docsScore.get(doc), 10) / 10.0;

                    // The new score is the weighted sum of the previous values
                    double new_score = (weight * norm_run_score) + norm_doc_score;
                    newTopicValues.put(doc, new_score);
                }


                // We use a LinkedHashMap in order to maintain the docs sorted
                Map<String, Double> sortedMap = new LinkedHashMap<>();

                // Sort the map by score in a decreasing order
                newTopicValues.entrySet()
                        .stream()
                        .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                        .forEachOrdered(x -> sortedMap.put(x.getKey(), x.getValue()));


                // Write the re-ranked run
                int rank_pos = 1;
                for (String doc : sortedMap.keySet()) {
                    double new_score = sortedMap.get(doc);
                    writer.printf(Locale.ENGLISH, "%s\t%s\t%d\t%.6f\t%s%n", topicID, doc, rank_pos, new_score, newRunID);
                    rank_pos++;
                }
            }

            buffer.close();
            writer.close();

            long elapsedTime = System.currentTimeMillis() - start;

            System.out.printf("%d topic(s) re-ranked in %d seconds.\n", numTopics, elapsedTime / 1000);

            System.out.printf("%n#### Re-ranking complete ####%n");

        } catch (FileNotFoundException e ) {
            LOGGER.error("Unable to find the run file.", e);
            throw new IllegalStateException("Unable to find the run file.", e);
        } catch (IOException e) {
            LOGGER.error("Unable to write the new re-ranked run.", e);
            throw new IllegalStateException("Unable to write the new re-ranked run.", e);
        }
    }


    /**
     * Main method of the class. Just for testing purposes.
     *
     * @param args command line arguments.
     */
    public static void main(String[] args) {

        // Increase Entity Size Limit to the max value
        System.setProperty("jdk.xml.totalEntitySizeLimit", String.valueOf(Integer.MAX_VALUE));

        final String docsPath = "./data/collection/Posts.V1.2.xml";
        final String runPath = "./experiment";

        final String runID = "GoogolFuel-task1-S41-auto-both-P";

        final int maxDocsRetrieved = 1000;
        final int expectedTopics = 100;

        ARQMathReRanker rr = new ARQMathReRanker(docsPath, runPath, runID, expectedTopics, maxDocsRetrieved);

        rr.reRanking(8);
    }
}