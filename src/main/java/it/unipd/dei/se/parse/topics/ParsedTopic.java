package it.unipd.dei.se.parse.topics;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * Represents a parsed topic to be indexed.
 *
 * @author GoogolFuel
 * @version 1.00
 * @since 1.00
 */
public class ParsedTopic {

    /**
     * The unique topic identifier.
     */
    private final String id;

    /**
     * The title of the topic.
     */
    private final String title;

    /**
     * The question of the topic.
     */
    private final String question;

    /**
     * The tags of the topic.
     */
    private final String tags;


    /**
     * Creates a new parsed topic
     *
     * @param id   the unique topic identifier.
     * @param title the title of the document.
     * @param question the question of the topic.
     * @param tags the tags of the topic.
     * @throws NullPointerException  if {@code id} and/or {@code body} are {@code null}.
     * @throws IllegalStateException if {@code id} and/or {@code body} are empty.
     */
    public ParsedTopic(final String id, final String title, final String question, final String tags) {

        if (id == null) {
            throw new NullPointerException("Topic identifier cannot be null.");
        }

        if (id.isEmpty()) {
            throw new IllegalStateException("Topic identifier cannot be empty.");
        }

        this.id = id;


        if (title == null) {
            throw new IllegalStateException("Topic title cannot be null.");
        }

        if (title.isEmpty()) {
            throw new IllegalStateException("Topic title cannot be empty.");
        }

        this.title = title;


        if (question == null) {
            throw new NullPointerException("Topic question cannot be null.");
        }

        if (question.isEmpty()) {
            throw new IllegalStateException("Topic question cannot be empty.");
        }

        this.question = question;


        if (tags == null) {
            throw new NullPointerException("Topic tags cannot be null.");
        }

        if (tags.isEmpty()) {
            throw new IllegalStateException("Topic tags cannot be empty.");
        }

        this.tags = tags;

    }


    /**
     * Returns the unique topic identifier.
     *
     * @return the unique topic identifier.
     */
    public String getId() {
        return id;
    }

    /**
     * Returns the title of the topic.
     *
     * @return the title of the topic.
     */
    public String getTitle() {
        return title;
    }

    /**
     * Returns the question of the topic.
     *
     * @return the question of the topic.
     */
    public String getQuestion() {
        return question;
    }

    /**
     * Returns the tags of the topic.
     *
     * @return the tags of the topic.
     */
    public String getTags() {
        return tags;
    }


    @Override
    public final String toString() {

        ToStringBuilder tsb = new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("Id", id)
                .append("Title", title)
                .append("Question", question)
                .append("Tags", tags);

        return tsb.toString();
    }

    @Override
    public final boolean equals(Object o) {
        return (this == o) || ((o instanceof ParsedTopic) && id.equals(((ParsedTopic) o).id));
    }

    @Override
    public final int hashCode() {
        return 37 * id.hashCode();
    }

}