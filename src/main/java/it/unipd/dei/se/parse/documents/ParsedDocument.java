package it.unipd.dei.se.parse.documents;

import org.apache.lucene.document.Field;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * Represents a parsed document to be indexed.
 *
 * @author GoogolFuel
 * @version 1.00
 * @since 1.00
 */
public class ParsedDocument {

    /**
     * The names of the {@link Field}s within the index.
     *
     * @author GoogolFuel
     * @version 1.00
     * @since 1.00
     */
    public final static class FIELDS {

        /**
         * The name of the element containing the document identifier.
         */
        public static final String ID = "id";

        /**
         * The name of the element containing the document body.
         */
        public static final String BODY = "body";

    }

    /**
     * The unique document identifier.
     */
    private final String id;

    /**
     * The body of the document.
     */
    private final String body;

    /**
     * The post type id of the document.
     */
    private final String postTypeId;

    /**
     * The score of the document.
     */
    private final String score;

    /**
     * The title of the document.
     */
    private final String title;

    /**
     * The tags of the document.
     */
    private final String tags;

    /**
     * The unique document parent identifier.
     */
    private final String parentId;

    /**
     * Creates a new parsed document
     *
     * @param id   the unique document identifier.
     * @param postTypeId the post type id of the document.
     * @param body the body of the document.
     * @param score the score of the document.
     * @param title the title of the document.
     * @param tags the tags of the document.
     * @param parentId the unique document parent identifier.
     * @throws NullPointerException  if {@code id} and/or {@code body} are {@code null}.
     * @throws IllegalStateException if {@code id} and/or {@code body} are empty.
     */
    public ParsedDocument(final String id, final String postTypeId, final String body, final String score, final String title, final String tags, final String parentId) {

        if (id == null) {
            throw new NullPointerException("Document identifier cannot be null.");
        }

        if (id.isEmpty()) {
            throw new IllegalStateException("Document identifier cannot be empty.");
        }

        this.id = id;


        if (postTypeId == null) {
            throw new IllegalStateException("Document postTypeId cannot be null.");
        }

        if (postTypeId.isEmpty()) {
            throw new IllegalStateException("Document postTypeId cannot be empty.");
        }

        this.postTypeId = postTypeId;


        if (body == null) {
            throw new NullPointerException("Document body cannot be null.");
        }

        if (body.isEmpty()) {
            throw new IllegalStateException("Document body cannot be empty.");
        }

        this.body = body;


        if (score == null) {
            throw new NullPointerException("Document score cannot be null.");
        }

        if (score.isEmpty()) {
            throw new IllegalStateException("Document score cannot be empty.");
        }

        this.score = score;

        if (this.postTypeId.equals("1")) {
            // ParsedDocument is a Question

            if (title == null) {
                throw new NullPointerException("Document title cannot be null.");
            }

            if (title.isEmpty()) {
                throw new IllegalStateException("Document title cannot be empty.");
            }

            this.title = title;

            if (tags == null) {
                throw new NullPointerException("Document tags cannot be null.");
            }

            if (tags.isEmpty()) {
                throw new IllegalStateException("Document tags cannot be empty.");
            }

            this.tags = tags;

            this.parentId = "";

        } else {
            // ParsedDocument is an Answer

            this.title = "";

            this.tags = "";

            if (parentId == null) {
                throw new NullPointerException("Document parent identifier cannot be null, if post type id is 2.");
            }

            if (parentId.isEmpty()) {
                throw new IllegalStateException("Document parent identifier cannot be empty, if post type id is 2.");
            }

            this.parentId = parentId;
        }

    }


    /**
     * Returns the unique document identifier.
     *
     * @return the unique document identifier.
     */
    public String getId() {
        return id;
    }

    /**
     * Returns the post type id of the document.
     *
     * @return the post type id of the document.
     */
    public String getPostTypeId() {
        return postTypeId;
    }

    /**
     * Returns the body of the document.
     *
     * @return the body of the document.
     */
    public String getBody() {
        return body;
    }

    /**
     * Returns the score of the document.
     *
     * @return the score of the document.
     */
    public String getScore() {
        return score;
    }

    /**
     * Returns the title of the document.
     *
     * @return the title of the document.
     */
    public String getTitle() {
        return title;
    }

    /**
     * Returns the tags of the document.
     *
     * @return the tags of the document.
     */
    public String getTags() {
        return tags;
    }

    /**
     * Returns the unique document parent identifier of the document.
     *
     * @return the unique document parent identifier of the document.
     */
    public String getParentId() {
        return parentId;
    }


    @Override
    public final String toString() {

        ToStringBuilder tsb;

        if(postTypeId.equals("1")) {
            // ParsedDocument is a Question
            tsb = new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                    .append("TYPE OF DOCUMENT", "Question")
                    .append("Id", id)
                    .append("Body", body)
                    .append("Score", score)
                    .append("Title", title)
                    .append("Tags", tags);
        } else {
            // ParsedDocument is an Answer
            tsb = new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                    .append("TYPE OF DOCUMENT", "Answer")
                    .append("Id", id)
                    .append("Body", body)
                    .append("Score", score)
                    .append("ParentId", parentId);
        }

        return tsb.toString();
    }

    @Override
    public final boolean equals(Object o) {
        return (this == o) || ((o instanceof ParsedDocument) && id.equals(((ParsedDocument) o).id));
    }

    @Override
    public final int hashCode() {
        return 37 * id.hashCode();
    }

}