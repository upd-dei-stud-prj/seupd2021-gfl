package it.unipd.dei.se.parse.documents;


import javax.xml.namespace.QName;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.Reader;


/**
 * The class for parsing XML documents.
 *
 * @author GoogolFuel
 * @version 1.00
 * @since 1.00
 */
public class XMLDocumentParser extends DocumentParser {

    /**
     * The currently parsed XML event
     */
    private XMLEvent event = null;

    /**
     * The name of the element containing the document.
     */
    private static final String ROW_ELEMENT = "row";

    /**
     * The name of the attribute containing the document identifier.
     */
    private static final String ID_ATTRIBUTE = "Id";

    /**
     * The name of the attribute containing the document post type id ("1": Question, "2": Answer).
     */
    private static final String POST_TYPE_ID_ATTRIBUTE= "PostTypeId";

    /**
     * The name of the attribute containing the document body.
     */
    private static final String BODY_ATTRIBUTE = "Body";

    /**
     * The name of the attribute containing the document score.
     */
    private static final String SCORE_ATTRIBUTE = "Score";

    /**
     * The name of the attribute containing the document title.
     */
    private static final String TITLE_ATTRIBUTE = "Title";

    /**
     * The name of the attribute containing the document tags.
     */
    private static final String TAGS_ATTRIBUTE = "Tags";

    /**
     * The name of the attribute containing the document parent identifier (present only if PostTypeId is "2")
     */
    public static final String PARENT_ID_ATTRIBUTE = "ParentId";


    /**
     * Creates a new XML document parser.
     *
     * @param in the reader to the document(s) to be parsed.
     *
     * @throws NullPointerException if {@code in} is {@code null}.
     * @throws IllegalStateException if any error occurs while creating the parser.
     */
    public XMLDocumentParser(final Reader in) {
        super(in);
    }


    @Override
    public boolean hasNext() {

        try {

            // Assume that there are no more elements
            next = false;

            while (in.hasNext()) {

                // Take the next event
                event = in.nextEvent();

                if (event.isStartElement()) {
                    // Within an open tag
                    StartElement startElement = event.asStartElement();
                    if (startElement.getName().getLocalPart().equals(ROW_ELEMENT)) {
                        // Row open tag, hence there is another document to parse
                        next = true;
                        break;
                    }
                }
            }

        } catch (XMLStreamException e) {
            LOGGER.error("Unable to parse the XML document.", e);
            throw new IllegalStateException("Unable to parse the XML document.", e);
        }

        return next;
    }


    @Override
    protected final ParsedDocument parse() {

        String id;
        String postType;
        String body;
        String score;
        String title = null;
        String tags = null;
        String parentId = null;

        // Get the current element
        StartElement startElement = event.asStartElement();

        // Read and store the attributes
        id = startElement.getAttributeByName(new QName(ID_ATTRIBUTE)).getValue();
        postType = startElement.getAttributeByName(new QName(POST_TYPE_ID_ATTRIBUTE)).getValue();
        body = startElement.getAttributeByName(new QName(BODY_ATTRIBUTE)).getValue();
        score = startElement.getAttributeByName(new QName(SCORE_ATTRIBUTE)).getValue();
        if (postType.equals("1")) {
            title = startElement.getAttributeByName(new QName(TITLE_ATTRIBUTE)).getValue();
            // Delete all the angular parenthesis inside the tags
            tags = startElement.getAttributeByName(new QName(TAGS_ATTRIBUTE)).getValue().replaceAll("<","").replaceAll(">", " ");
        } else {
            parentId = startElement.getAttributeByName(new QName(PARENT_ID_ATTRIBUTE)).getValue();
        }

        return new ParsedDocument(id, postType, body, score, title, tags, parentId);
    }

}