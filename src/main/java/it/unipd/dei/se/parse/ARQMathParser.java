package it.unipd.dei.se.parse;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.Reader;

import java.io.FileWriter;
import java.io.PrintWriter;
import java.io.IOException;

import it.unipd.dei.se.parse.documents.XMLDocumentParser;
import it.unipd.dei.se.parse.documents.ParsedDocument;
import it.unipd.dei.se.parse.topics.XMLTopicParser;
import it.unipd.dei.se.parse.topics.ParsedTopic;

/**
 * The class for testing the parsers.
 *
 * @author GoogolFuel
 * @version 1.00
 * @since 1.00
 */
public class ARQMathParser {

    /**
     * Main method of the class. Just for testing purposes.
     *
     * @param args command line arguments.
     *
     * @throws IOException if something goes wrong while loading the file(s).
     * @throws IllegalStateException if something goes wrong while parsing the documents/topics.
     */
    public static void main(String[] args) throws IllegalStateException, IOException {

        // Increase Entity Size Limit to the max value
        System.setProperty("jdk.xml.totalEntitySizeLimit", String.valueOf(Integer.MAX_VALUE));

        // Documents parsing

        /*Reader file = new FileReader("./data/collection/Posts.V1.2.xml");

        XMLDocumentParser docParser = new XMLDocumentParser(file);
        int k = 0;
        for (ParsedDocument d : docParser) {
            System.out.printf("%n%n------------------------------------%n%s%n%n%n", d.toString());
            //if(d.getPostTypeId().equals("2")) k++;
            //k++;
        }

        System.out.println("Number of parsed documents: " + k);*/


        // Topics parsing

        //Reader file = new FileReader("./data/topics/Topics2020.xml");
        Reader file = new FileReader("./data/topics/Topics2021.xml");

        XMLTopicParser topParser = new XMLTopicParser(file);
        int k = 0;
        for (ParsedTopic t : topParser) {
            System.out.printf("%n%n------------------------------------%n%s%n%n%n", t.toString());
            k++;
        }

        System.out.println("Number of parsed topics: " + k);

    }

}