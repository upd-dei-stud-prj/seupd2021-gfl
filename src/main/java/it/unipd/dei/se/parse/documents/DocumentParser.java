package it.unipd.dei.se.parse.documents;

import java.io.Reader;
import java.util.Iterator;
import java.util.NoSuchElementException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;


/**
 * Represents an abstract document parser
 *
 * @author GoogolFuel
 * @version 1.00
 * @since 1.00
 */
public abstract class DocumentParser implements Iterator<ParsedDocument>, Iterable<ParsedDocument> {

    /**
     * A LOGGER available for all the subclasses.
     */
    protected static final Logger LOGGER = LogManager.getLogger(DocumentParser.class);

    /**
     * Indicates whether there is another {@code ParsedDocument} to return.
     */
    protected boolean next;

    /**
     * The reader to be used to parse document(s).
     */
    protected XMLEventReader in;


    /**
     * Creates a new document parser.
     *
     * @param in the reader to the document(s) to be parsed.
     * @throws NullPointerException if {@code in} is {@code null}.
     * @throws IllegalStateException if any error occurs while creating the parser.
     */
    protected DocumentParser(final Reader in) {

        if (in == null) {
            throw new NullPointerException("Reader cannot be null.");
        }

        XMLInputFactory XIF = XMLInputFactory.newInstance();
        XIF.setProperty(XMLInputFactory.IS_COALESCING, true);

        try {
            this.in = XIF.createXMLEventReader(in);
        } catch (XMLStreamException e) {
            LOGGER.error("Unable to instantiate the XML document parser.");
            throw new IllegalStateException("Unable to instantiate the XML document parser.");
        }
    }

    @Override
    public final Iterator<ParsedDocument> iterator() {
        return this;
    }

    @Override
    public boolean hasNext() {
        return next;
    }

    @Override
    public final ParsedDocument next() {

        if (!next) {
            throw new NoSuchElementException("No more documents to parse.");
        }

        try {
            return parse();
        } finally {
            try {
                // we reached the end of the file
                if (!next) {
                    in.close();
                }
            } catch (XMLStreamException e) {
                LOGGER.error("Unable to parse the XML document.", e);
                throw new IllegalStateException("Unable to close the reader.", e);
            }
        }

    }

    /**
     * Creates a new {@code DocumentParser}.
     *
     * It assumes the {@code DocumentParser} has a single-parameter constructor which takes a {@code Reader} as input.
     *
     * @param cls the class of the document parser to be instantiated.
     * @param in  the reader to the document(s) to be parsed.
     * @return a new instance of {@code DocumentParser} for the given class.
     * @throws NullPointerException  if {@code cls} and/or {@code in} are {@code null}.
     * @throws IllegalStateException if something goes wrong in instantiating the class.
     */
    public static DocumentParser create(Class<? extends DocumentParser> cls, Reader in) {

        if (cls == null) {
            throw new NullPointerException("Document parser class cannot be null.");
        }

        if (in == null) {
            throw new NullPointerException("Reader cannot be null.");
        }

        try {
            return cls.getConstructor(Reader.class).newInstance(in);
        } catch (Exception e) {
            String error = String.format("Unable to instantiate document parser %s.", cls.getName());
            LOGGER.error(error, e);
            throw new IllegalStateException(error, e);
        }

    }

    /**
     * Performs the actual parsing of the document.
     *
     * @return the parsed document.
     */
    protected abstract ParsedDocument parse();

}