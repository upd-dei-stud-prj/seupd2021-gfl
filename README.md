# ARQMath 2021 Task 1: Answer Retrieval by GoogolFuel

AUTHORS:

- **Bettin Manuel**, ID: `1242413`
- **Bortolami Gianmarco**, ID: `1239293`
- **Bruno Davide**, ID: `1231831`
- **Dainese Matteo**, ID: `1238165`
- **Galvan Elena**, ID: `1236458`
- **Scicchitano Wiliam**, ID: `1241980`

#### The code was developed all together during long Zoom video callings due to the fact that the all the phases depend on one to each other, furthermore the task has a sequential behaviour. As a consequence, almost all the commits are performed by one member of the group at the end of a call.

---------------------
## Project organization

---------------------
```
- seupd2021-gfl
    |
    |- data
        |- collection
            |- Posts.V1.2.xml (to download from https://drive.google.com/file/d/1Lwj6L6VJuo2HEz7dY-hXumtELRGkDCC3/view?usp=sharing)
        |- resources
            |- okapi.txt
        |- topics
            |- arqmath-format-results-2020.tsv (created by Tangent-S)
            |- arqmath-format-results-2021.tsv (created by Tangent-S)
            |- Topics2020_trec.txt (created by the code)
            |- Topics2020.xml
            |- Topics2021_trec.txt (created by the code)
            |- Topics2021.xml
    |
    |- experiment
            |- index (created by the code)
            |
            |- GoogolFuel-task1-S41-auto-both-A.tsv
            |- GoogolFuel-task1-S41R71-auto-both-P.tsv
            |- GoogolFuel-task1-S41R81-auto-both-A.tsv
            |- GoogolFuel-task1-S41R91-auto-both-A.tsv
            |- GoogolFuel-task1-S51-auto-both-A.tsv
            |- GoogolFuel-task1-S51R71-auto-both-A.tsv
            |- GoogolFuel-task1-S51R81-auto-both-A.tsv
            |- GoogolFuel-task1-S51R91-auto-both-A.tsv
    |
    |- javadoc (created by Maven javadoc:javadoc plugin)
    |
    |- src/main/java/it/unipd/dei/se
            |- analysis
                |- AnalyzerUtil.java
                |- ARQMathAnalyzer.java               
            |
            |- index
                |- ARQMathIndexer.java
                |- BodyField.java
            |
            |- parse
                |- documents
                    |- DocumentParser.java
                    |- ParsedDocument.java
                    |- XMLDocumentParser.java
                |
                |- topics
                    |- ParsedTopic.java
                    |- TopicParser.java
                    |- XMLTopicParser.java
                |- ARQMathParser.java
            |
            |- search
                |- ARQMathReRanker.java
                |- ARQMathSearcher.java
            |
            |- ARQMath.java
    |
    |- pom.xml
    |
    |- gfl-task1-report.pdf
    |
    |- README.md
```